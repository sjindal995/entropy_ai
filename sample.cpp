#include <iostream>
#include <limits>
#include <vector>
#include <algorithm>
using namespace std;

bool compare(int a,int b){
	return (a<b);
}

int main(){
	// cout << numeric_limits<long long int>::min() << endl;
	// cout << numeric_limits<long long int>::max() << endl;
	vector<int> a;
	a.push_back(1);
	a.push_back(2);
	a.push_back(3);
	a.push_back(4);
	sort(a.rbegin(),a.rend(),compare);
	for(int i=0;i<a.size();i++){
		cout << a.at(i) << endl;
	}
}
