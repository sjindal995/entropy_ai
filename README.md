# The Game of Entropy(N): #

**Setup:**
The game board is NxN. There are N colors and N tiles of each color. Typical values for N are 5 and 7.

**Objective:**
There are 2 players, Order and Chaos. Order's objective is to move colored tiles on the board such that the number of palindromes made on the game board when all tiles have been placed is maximized. Chaos's objective is to place these colored tiles on the board such that this is prevented.

A palindrome is a sequence of tiles (in the horizontal or vertical direction) that is exactly the same when looked at from either end. Some examples of valid palindromes (assuming (R)ed, (B)lue, (G)reen as tile colors) are RGBGR, RRR, RR, GGBGG. Note that we will not be considering diagonal moves, or scoring in this game.

**Player Roles:**

**Chaos** - On each turn, Chaos randomly draws a single tile from the bag of remaining tiles and places it on an empty space of his/her choice on the game board. For our game, the server will be giving Chaos a colored tile drawn uniformly at random from the bag of remaining tiles.

**Order** - On each turn, Order is allowed to move a single tile already on the game board, either horizontally or vertically (not diagonally). The tile can be moved an arbitrary number of spaces in either direction (>= 1) as long as no tile is jumped over (i.e. occupied slots on the board block tile movement).

The game proceeds turn by turn, beginning with Chaos. When all tiles have been placed on the board, the total score is calculated, and then the players switch sides. The player scoring higher as Order wins.

**Scoring:**

All palindromes of length >= 2 are counted. Some examples,

GGGGG = 5 + 4*2 + 3*3 + 2*4 = 30

Explanation - All contiguous substrings are palindromes.

RGBGR = 5 + 3 = 8

Explanation - GBG is a substring palindrome of length 3.

RGRGR = 5 + 3*3 = 14

Explanation - RGRGR/RGR/GRG/RGR.