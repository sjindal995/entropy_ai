#include <iostream>
#include <cstring>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <limits>
#include <map>

using namespace std;

string board="";
int n=5;
int tiles_colored=0;
string role="";
map<string,vector<double> > min_node_map;
map<string,vector<double> > max_node_map;
map<string,double> score_map;

bool makeOrderMove(int a,int b,int c,int d);
void printBoard();

void printX(string message){
	cerr << message << "\n";
}

bool isGameOver(){
	for(int i=0;i<n;i++){
		for(int j=0;j<n;j++){
			if(board[n*i + j]=='-') return false;
		}
	}
	return true;
}

vector<int*> getPossibleOrderMoves(int x,int y){
	vector<int*> possibleMoves;
	int* a1 = new int[2];
	a1[0]=x;
	a1[1]=y;
	possibleMoves.push_back(a1);
	for(int i=x-1;i>-1;i--){
		if(board[n*i + y]=='-'){
			int* a = new int[2];
			a[0] = i;
			a[1] = y;
			possibleMoves.push_back(a);
		}
		else break;
	}
	for(int i=y-1;i>-1;i--){
		if(board[n*x + i]=='-'){
			int* a = new int[2];
			a[0] = x;
			a[1] = i;
			possibleMoves.push_back(a);
		}
		else break;
	}
	for(int i=x+1;i<n;i++){
		if(board[n*i + y]=='-'){
			int* a = new int[2];
			a[0] = i;
			a[1] = y;
			possibleMoves.push_back(a);
		}
		else break;
	}
	for(int i=y+1;i<n;i++){
		if(board[n*x + i]=='-'){
			int* a = new int[2];
			a[0] = x;
			a[1] = i;
			possibleMoves.push_back(a);
		}
		else break;
	}
	return possibleMoves;
}

bool inBounds(int x,int y){
	return ((x>=0)&&(x<n)&&(y>=0)&&(y<n));
}


double scoreListH(string list,vector<int> colors_left_vec){
	double score=0;
	int length=0;
	int left=0;
	int right=0;
	double tmpScore=0;
	double tmpScore2=0;
	bool dashes=false;
	if(score_map[list]){
		return score_map[list];
	}
	int total_colors_left=0;
	for(int j=0;j<colors_left_vec.size();j++){
		total_colors_left += colors_left_vec.at(j);
	}
	for(int i=1;i<list.size();i++){
		//palindrome of even length
		length=0;
		left=i-1;
		right=i;
		tmpScore=0;
		tmpScore2=0;
		while(inBounds(left,right)){
			if((list[left]==list[right])&&(list[left]!='-')) tmpScore += (length+2);
			else if((list[left]==list[right])&&(list[left]=='-')){
				dashes=true;
				for(int j=0;j<colors_left_vec.size();j++){
					if(colors_left_vec.at(j)>1){
						double prob=0;
						if (total_colors_left>0)
							prob = (colors_left_vec.at(j)*(colors_left_vec.at(j)-1))/((total_colors_left)*(total_colors_left-1));
						list[left] = j+65;
						list[right] = j+65;
						colors_left_vec.at(j) -= 2;
						if(score_map[list]){
							// cerr << "mapped : " << list << "   :::: "  << score_map[list]<<endl;
							tmpScore2 += prob*score_map[list];
						}
						else tmpScore2 += prob*(scoreListH(list,colors_left_vec));
						list[left] = '-';
						list[right] = '-';
						colors_left_vec.at(j) += 2;
					}
				}
			}
			else if((list[left]!=list[right])&&((list[left]=='-')||(list[right]=='-'))){
				dashes=true;
				string changed;
				if(list[left]=='-'){
					changed = "left";
					list[left]=list[right];
				}
				else{
					changed = "right";
					list[right]=list[left];
				}
				if(score_map[list]){
					// cerr << "mapped : " << list << "   :::: "  << score_map[list]<<endl;
					if(total_colors_left>0)
						tmpScore2 += ((colors_left_vec.at(list[left]-65))/float(total_colors_left))*score_map[list];
				}
				if(colors_left_vec.at(list[left]-65) > 0){
					colors_left_vec.at(list[left]-65)--;
					if(total_colors_left>0)
						tmpScore2 += ((colors_left_vec.at(list[left]-65)+1)/float(total_colors_left))*(scoreListH(list,colors_left_vec));
				}
				if(changed=="left") list[left]='-';
				else list[right]='-';
			}
			else break;
			length +=2;
			right +=1;
			left -=1;
		}
		// if(dashes) score += tmpScore2;
		// else score += tmpScore;
		// //palindromes with odd length
		length=1;
		left=i-1;
		right=i+1;
		// tmpScore=0;
		// tmpScore2=0;
		// dashes=false;
		while(inBounds(left,right)){
			if((list[left]==list[right])&&(list[left]!='-')) tmpScore += (length+2);
			else if((list[left]==list[right])&&(list[left]=='-')){
				dashes=true;
				for(int j=0;j<colors_left_vec.size();j++){
					if(colors_left_vec.at(j)>1){
						double prob=0;
						if(total_colors_left>0)
							prob = (colors_left_vec.at(j)*(colors_left_vec.at(j)-1))/((total_colors_left)*(total_colors_left-1));
						list[left] = j+65;
						list[right] = j+65;
						colors_left_vec.at(j) -= 2;
						if(score_map[list]){
							// cerr << "mapped : " << list << "   :::: "  << score_map[list]<<endl;
							tmpScore2 += prob*score_map[list];
						}
						else tmpScore2 += prob*(scoreListH(list,colors_left_vec));
						list[left] = '-';
						list[right] = '-';
						colors_left_vec.at(j) += 2;
					}
				}
			}
			else if((list[left]!=list[right])&&((list[left]=='-')||(list[right]=='-'))){
				dashes=true;
				string changed;
				if(list[left]=='-'){
					changed = "left";
					list[left]=list[right];
				}
				else{
					changed = "right";
					list[right]=list[left];
				}
				if(score_map[list]){
					// cerr << "mapped : " << list << "   :::: "  << score_map[list]<<endl;
					if(total_colors_left>0)
						tmpScore2 += ((colors_left_vec.at(list[left]-65))/float(total_colors_left))*score_map[list];
				}
				if(colors_left_vec.at(list[left]-65) > 0){
					colors_left_vec.at(list[left]-65)--;
					if(total_colors_left>0)
						tmpScore2 += ((colors_left_vec.at(list[left]-65)+1)/float(total_colors_left))*(scoreListH(list,colors_left_vec));
				}
				if(changed=="left") list[left]='-';
				else list[right]='-';
			}
			else break;
			length +=2;
			right +=1;
			left -=1;
		}
		if(dashes) score += tmpScore2;
		else score += tmpScore;
	}
	// if(score!=0) cerr << "score : " << score <<endl;
	if(!score_map[list]){
		// cerr << "inserting : " << list << " ::::: " << score << endl;
		score_map[list] = score;
	}
	return score;
}

double scoreListD(string list){
	double score=0;
	int length=0;
	int left=0;
	int right=0;
	double tmpScore=0;
	int dash_count=0;
	// int total_colors_left=0;
	// for(int j=0;j<colors_left_vec.size();j++){
	// 	total_colors_left += colors_left_vec.at(j);
	// }
	for(int i=1;i<list.size();i++){
		//palindrome of even length
		length=0;
		left=i-1;
		right=i;
		tmpScore=0;
		dash_count=0;
		while((inBounds(left,right))){
			if(list[left]=='-') dash_count++;
			if(list[right]=='-') dash_count++;

			if((list[left]==list[right])&&(list[left]!='-')) tmpScore += (length+2);
			else if((list[left]!=list[right])&&(list[left]!='-')&&(list[right]!='-')) break;
			else if((list[left]!=list[right])&&(list[left]=='-')){
				tmpScore += (length+2)/float(dash_count+1);
				// if(total_colors_left>0) tmpScore += (length+2)*(colors_left_vec.at(list[right]-65))/float(total_colors_left);
			}
			else if((list[left]!=list[right])&&(list[right]=='-')){
				tmpScore += (length+2)/float(dash_count+1);
				// if(total_colors_left>0) tmpScore += (length+2)*(colors_left_vec.at(list[left]-65))/float(total_colors_left);
			}
			else{
				tmpScore += (length+2)/float(3*(dash_count+1));
				// if(total_colors_left>1){
				// 	double chances=0;
				// 	for(int j=0;j<n;j++){
				// 		if(colors_left_vec.at(j)>1){
				// 			chances += (colors_left_vec.at(j)*(colors_left_vec.at(j)-1))/2.0;
				// 		}
				// 	}
				// 	chances = (chances*2)/float(total_colors_left*(total_colors_left-1));
				// 	tmpScore += (length+2)*(chances);
				// }
			}
			length +=2;
			right +=1;
			left -=1;
		}
		// left++;
		// right--;
		// if(list[left]!='-') score += tmpScore/float(dash_count+1);
		score += tmpScore;
		// //palindromes with odd length
		length=1;
		left=i-1;
		right=i+1;
		tmpScore=0;
		dash_count=0;
		while((inBounds(left,right))){
			if(list[left]=='-') dash_count++;
			if(list[right]=='-') dash_count++;

			if((list[left]==list[right])&&(list[left]!='-')) tmpScore += (length+2);
			else if((list[left]!=list[right])&&(list[left]!='-')&&(list[right]!='-')) break;
			else if((list[left]!=list[right])&&(list[left]=='-')){
				tmpScore += (length+2)/float(dash_count+1);
				// if(total_colors_left>0) tmpScore += (length+2)*(colors_left_vec.at(list[right]-65))/float(total_colors_left);
			}
			else if((list[left]!=list[right])&&(list[right]=='-')){
				tmpScore += (length+2)/float(dash_count+1);
				// if(total_colors_left>0) tmpScore += (length+2)*(colors_left_vec.at(list[left]-65))/float(total_colors_left);
			}
			else{
				tmpScore += (length+2)/float(3*(dash_count+1));
				// if(total_colors_left>1){
				// 	double chances=0;
				// 	for(int j=0;j<n;j++){
				// 		if(colors_left_vec.at(j)>1){
				// 			chances += (colors_left_vec.at(j)*(colors_left_vec.at(j)-1))/2.0;
				// 		}
				// 	}
				// 	chances = (chances*2)/float(total_colors_left*(total_colors_left-1));
				// 	tmpScore += (length+2)*(chances);
				// }
			}
			length +=2;
			right +=1;
			left -=1;
		}
		// left++;
		// right--;
		// if(list[left]!='-') score += tmpScore/float(dash_count+1);
		score += tmpScore;
	}
	// if(score!=0) cerr << "score : " << score <<endl;
	return score;
}


double scoreList(string list){
	double score=0;
	int length=0;
	int left=0;
	int right=0;
	double tmpScore=0;
	int dash_count=0;
	for(int i=1;i<list.size();i++){
		//palindrome of even length
		length=0;
		left=i-1;
		right=i;
		tmpScore=0;
		dash_count=0;
		while((inBounds(left,right))&&(list[left]==list[right])){
			tmpScore += (length+2);
			if(list[left]=='-') dash_count++;
			if(list[right]=='-') dash_count++;
			length +=2;
			right +=1;
			left -=1;
		}
		left++;
		right--;
		if(list[left]!='-') score += tmpScore/float(dash_count+1);
		// //palindromes with odd length
		length=1;
		left=i-1;
		right=i+1;
		tmpScore=0;
		dash_count=0;
		while((inBounds(left,right))&&(list[left]!='-')&&(list[left]==list[right])){
			tmpScore += (length+2);
			if(list[left]=='-') dash_count++;
			if(list[right]=='-') dash_count++;
			length +=2;
			right +=1;
			left -=1;	
		}
		left++;
		right--;
		if(list[left]!='-') score += tmpScore/float(dash_count+1);
	}
	// if(score!=0) cerr << "score : " << score <<endl;
	return score;
}

double scoreListO(string list){
	double score=0;
	int length=0;
	int left=0;
	int right=0;
	double tmpScore=0;
	for(int i=1;i<list.size();i++){
		//palindrome of even length
		length=0;
		left=i-1;
		right=i;
		tmpScore=0;
		while((inBounds(left,right))&&(list[left]!='-')&&(list[left]==list[right])){
			tmpScore += (length+2);
			length +=2;
			right +=1;
			left -=1;
		}
		score += tmpScore;
		//palindromes with odd length
		length=1;
		left=i-1;
		right=i+1;
		tmpScore=0;
		while((inBounds(left,right))&&(list[left]!='-')&&(list[left]==list[right])){
			tmpScore += (length+2);
			length +=2;
			right +=1;
			left -=1;	
		}
		score += tmpScore;
	}
	return score;
}

double scoreBoard(){
	double score=0;
	string row="";
	// vector<int> colors_left_vec;
	// for(int i=0;i<n;i++) colors_left_vec.push_back(0);
	// for(int i=0;i<n;i++){
	// 	for(int j=0;j<n;j++){
	// 		if(board[n*i + j]!='-') colors_left_vec.at(board[n*i + j]-65)++;
	// 	}
	// }
	// int total_colors_left = 0;
	// for(int i=0;i<n;i++){
	// 	colors_left_vec.at(i) = n-colors_left_vec.at(i);
	// 	total_colors_left += colors_left_vec.at(i);
	// }
	for(int i=0;i<n;i++){
		row="";
		for(int j=0;j<n;j++) row += board[n*i + j];
		// if(total_colors_left<0){
		// 	score += scoreListH(row,colors_left_vec);
		// }
		// else
			score += scoreListO(row);
	}

	for(int i=0;i<n;i++){
		string col="";
		for(int j=0;j<n;j++){
			col += board[n*j + i];
		}
		// if(total_colors_left<0) score += scoreListH(col,colors_left_vec);
		// else
			score += scoreListO(col);
	}
	return score;
}

bool boardFilled(){
	for(int i=0;i<n;i++){
		for(int j=0;j<n;j++){
			if(board[n*i + j]=='-') return false;
		}
	}
	return true;
}

vector<int> colors_left(){
	vector<int> present;
	for(int i=0;i<n;i++) present.push_back(0);
	for(int i=0;i<n;i++){
		for(int j=0;j<n;j++){
			if(board[n*i + j]!='-')
				present.at(int(board[n*i + j]) -65)++;
		}
	}
	for(int i=0;i<n;i++) present.at(i) = n-present.at(i);
	return (present);
}


bool compare_score(int* a,int* b){
	return (a[0]<b[0]);
}


//0:min , 1:max, 2:chance
double expectiMiniMax(int depth, int* turn_color, double alpha, double beta){
	double score=0;
	if((boardFilled())||(depth==0)) return scoreBoard();
	if(turn_color[0]==0){
		score = numeric_limits<int>::max();
		string state_color = board;
		state_color += turn_color[1];
		for(int i=0;i<n;i++){
			for(int j=0;j<n;j++){
				// cerr << "enter emm : " <<i << " , " << j <<"  "<< turn_color[1]<<endl;
				if(board[n*i + j]=='-'){
					board[n*i + j]= turn_color[1];
					int* a = new int;
					a[0] = 1;
					if((max_node_map[board]).size()!=0){
						if(max_node_map[board].at(0)>=depth-1){
							// cerr << "mapping board : "<<board << " :: " <<max_node_map[board].at(0) << " >= " << depth << endl;
							score = max_node_map[board].at(1);
							board[n*i + j]='-';
							// cerr << "exit emm : " << i << ", " << j<< " "  <<endl;
							return score;
						}
					}
					score = min(score,expectiMiniMax(depth-1,a, alpha, beta));
					board[n*i + j]='-';
					if(score<=alpha){
						vector<double> map_elem;
						map_elem.push_back(depth);
						map_elem.push_back(score);
						// cerr << "--insert element board : " << state_color << " :: " << map_elem.at(0) << " , " << map_elem.at(1) << endl;
						min_node_map[state_color] = map_elem;
						// cerr << "exit emm : " << i << ", " << j<< " "  << score<<endl;
						return score;
					}
					beta = min(beta, score);
				}
				// cerr << "exit emm : " << i << ", " << j<< " "<<endl;
			}
		}
		vector<double> map_elem;
		map_elem.push_back(depth);
		map_elem.push_back(score);
		// cerr << "--insert element board : " << state_color << " :: " << map_elem.at(0) << " , " << map_elem.at(1) << endl;
		min_node_map[state_color] = map_elem;
	}
	else if(turn_color[0]==1){
		score = numeric_limits<int>::min();
		for(int i=0;i<n;i++){
			for(int j=0;j<n;j++){
				if(board[n*i + j]!='-'){
					vector<int*> possibleMoves = getPossibleOrderMoves(i,j);
					for(int k=0;k<possibleMoves.size();k++){
						makeOrderMove(i,j,possibleMoves.at(k)[0],possibleMoves.at(k)[1]);
						int* a = new int;
						a[0] = 2;
						score = max(score,expectiMiniMax(depth-1,a, alpha, beta));
						makeOrderMove(possibleMoves.at(k)[0],possibleMoves.at(k)[1],i,j);
						
						if(score>=beta){
							vector<double> map_elem;
							map_elem.push_back(depth);
							map_elem.push_back(score);
							// cerr << "--insert element board : " << state_color << " :: " << map_elem.at(0) << " , " << map_elem.at(1) << endl;
							max_node_map[board] = map_elem;
							return score;
						}
						alpha = max(alpha, score);
					}
				}
			}
		}
		vector<double> map_elem;
		map_elem.push_back(depth);
		map_elem.push_back(score);
		// cerr << "--insert element board : " << state_color << " :: " << map_elem.at(0) << " , " << map_elem.at(1) << endl;
		max_node_map[board] = map_elem;
	}
	else if(turn_color[0]==2){
		score = 0;
		double prob;
		vector<int> colors_left_vec = colors_left();
		int total_colors_left =0;
		for(int k=0;k<n;k++){
			total_colors_left += colors_left_vec.at(k);
		}
		if(total_colors_left!=0){
			for(int k=0;k<n;k++){
				prob = (colors_left_vec.at(k)/float(total_colors_left));
				int* a = new int;
				a[0] = 0;
				a[1]=k+65;
				string state_color = board + char(a[1]);
				if((min_node_map[state_color]).size()!=0){
					if(min_node_map[state_color].at(0)>=depth-1){
						// cerr << "mapping board : "<<state_color << " :: " <<min_node_map[state_color].at(0) << " >= " << depth << endl;
						score += (prob*(min_node_map[state_color].at(1)));
					}
					else{
						// cerr << "--------------close----------"<<endl;
						score += (prob*expectiMiniMax(depth-1,a, alpha, beta));
					}
				}
				else{
					// cerr << "-------------outside------------"<<endl;
					score += (prob*expectiMiniMax(depth-1,a, alpha, beta));
				}
			}
		}
	}
	return score;
}


int* orderEmmAI(){
	// cerr << "------inside orderEmmAI----------"<<endl;
	double max_score = double(numeric_limits<int>::min());
	double score=max_score;
	int* ans = new int[4];
	int depth=5;
	// if(tiles_colored>11) depth=6;
	for(int i=0;i<n;i++){
		for(int j=0;j<n;j++){
			if(board[n*i + j]!='-'){
				vector<int*> possibleMoves = getPossibleOrderMoves(i,j);
				for(int k=0;k<possibleMoves.size();k++){
					// board[n*possibleMoves.at(k)[0] + possibleMoves.at(k)[1]] = board[n*i + j];
					// board[n*i + j]='-';
					makeOrderMove(i,j,possibleMoves.at(k)[0],possibleMoves.at(k)[1]);
					int* a = new int;
					a[0] = 2;
					score = expectiMiniMax(depth-1,a,numeric_limits<int>::min(),numeric_limits<int>::max());
					if(score>max_score){
						max_score=score;
						ans[0]=i;
						ans[1]=j;
						ans[2]=possibleMoves.at(k)[0];
						ans[3]=possibleMoves.at(k)[1];
					}
					makeOrderMove(possibleMoves.at(k)[0],possibleMoves.at(k)[1],i,j);
					// board[n*i + j] = board[n*possibleMoves.at(k)[0] + possibleMoves.at(k)[1]];
					// board[n*possibleMoves.at(k)[0] + possibleMoves.at(k)[1]]='-';
				}
			}
		}
	}
	vector<double> map_elem;
	map_elem.push_back(depth);
	map_elem.push_back(max_score);
	max_node_map[board]=map_elem;
	return ans;
}


int* chaosEmmAI(string color){
	double min_score = numeric_limits<int>::max();
	double score=min_score;
	int* ans = new int[2];
	int depth=5;
	// if(tiles_colored>11) depth=6;
	// cerr << "entering chaosEmmAI : " << board << endl;
	for(int i=0;i<n;i++){
		for(int j=0;j<n;j++){
			if(board[n*i + j]=='-'){
				board[n*i + j]= color[0];
				int* a = new int;
				a[0] = 1;
				score = expectiMiniMax(depth-1,a,numeric_limits<int>::min(),numeric_limits<int>::max());
				if(score<min_score){
					min_score=score;
					ans[0]=i;
					ans[1]=j;
				}
				board[n*i + j]='-';
			}
		}
	}
	vector<double> map_elem;
	map_elem.push_back(depth);
	map_elem.push_back(min_score);
	min_node_map[board+color]=map_elem;
	// cerr << "answer---------- : ::  " << board[n*ans[0] + ans[1]] << "  " << ans[0] << " " << ans[1]<<endl;
	// cerr << "exiting chaosEmmAI : " << board << endl;
	return ans;
}

///////////////////////////////////////////

void printBoard(){
	for(int x=0;x<n;x++){
		for(int y=0;y<n;y++){
			cerr << board[n*x + y] << " ";
		}
		cerr << "\n";
	}
	cerr << "\n";
}


bool makeOrderMove(int a,int b,int c,int d){
	board[n*c + d] = board[n*a + b];
	if((a!=c)||(b!=d)) board[n*a + b] = '-';
	return true;
}

void playAsOrder(){
	cerr << "ORDER\n";
	int max_tiles = n*n;
	while(1){
		// printBoard();
		int x;
		int y;
		string color;
		cin>>x;
		cin>>y;
		cin>>color;
		board[n*x + y]=color[0];
		tiles_colored++;
		if(tiles_colored>=max_tiles) return;
		// int* oi = orderAI();
		int* oi = orderEmmAI();
		int a = oi[0];
		int b = oi[1];
		int c = oi[2];
		int d = oi[3];
		makeOrderMove(a,b,c,d);
		cout << a << " " << b << " " << c << " " << d << endl;
	}
}

void playAsChaos(){
	cerr << "CHAOS\n";
	string color;
	cin>>color;
	// cerr << " board : "  << board << endl;
	int* ci = chaosEmmAI(color);
	int x = ci[0];
	int y = ci[1];
	// cerr << "move  : " << x << " " << y << " :: " << color[0]<< endl;
	board[n*x + y] = color[0];
	cout << x << " " << y << endl;
	printBoard();
	tiles_colored++;
	int max_tiles = n*n;
	while(1){
		// cerr << "tiles : " << tiles_colored  << "     " << board  << endl;
		if(tiles_colored>=max_tiles) return;
		int a,b,c,d;
		cin>>a>>b>>c>>d;
		// cerr << "input :: " << a << " " << b << " " << c << " " << d << endl;
		makeOrderMove(a,b,c,d);
		// cerr << "move  : " << a << " " << b  << " " << c << " "  << d<< endl;
		cin>>color;
		// ci = chaosAI(color);
		printBoard();
		ci = chaosEmmAI(color);
		x = ci[0];
		y = ci[1];
		board[n*x + y]=color[0];
		// cerr << "move  : " << x << " " << y << endl;
		// cerr << "my move :::::: " << x << " " << y << endl;
		printBoard();
		cout << x << " " << y << endl;
		tiles_colored++;
		cerr << "tiles_colored : " << tiles_colored << endl;
	}
}

int main(){
	srand(time(NULL));
	cin>>n;
	cin>>role;
	for(int i=0;i<n;i++){
		for(int j=0;j<n;j++){
			board += '-';
		}
	}
	if(role=="ORDER") playAsOrder();
	else if(role=="CHAOS") playAsChaos();
	else cerr << "I am not intelligent for this role: " << role;
	cerr << "--graceful exit by myAI--";
		// cerr << "------------inside map-----------"<<endl;
	// for (auto& x: min_node_map) {
 //    	cerr << x.first << ": " << x.second.at(0) << x.second.at(1) << endl;
 //  	}
	return 0;
}