#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <cstring>
#include <sstream>
#include <fstream>
#include <map>
#include <vector>
using namespace std;

int port = 10000;
bool server_started = false;
int board_size;
int max_clients = 2;
int nfeatures;
// double wts_vec[4] = {0.25,0.25,0.25,0.25};
vector<double> wts_vec;
map<string,double> vals;

map<string, vector<string> > neighbours;

void loadNeighbours(){
    
}

string float2str(double a){
    stringstream ss;
    ss << a;
    string ans = ss.str();
    return ans;
}

string hashf(vector<double> i_wts){
    string hashv = "";
    for(int i=0;i<nfeatures;i++) hashv += float2str(i_wts.at(i)) + ",";
    // if(hashv.length() > 0) hashv.pop_back();
    return hashv;
}

vector<double> hash2state(string hashv){
    // double* c_wts = new double[nfeatures];
    vector<double> c_wts;
    string cur="";
    int pos=0;
    for(int i=0;i<hashv.length();i++){
        if(hashv[i] == ','){
            c_wts.push_back(stof(cur.c_str()));
            cur="";
            pos++;
        }
        else cur += hashv[i];
    }
    return c_wts;
    // if(hashv.length()>0) c_wts[pos] = stof(cur);
}

void *serverThread(void* arg){
    port = (rand()%10000)+10000;
    cout << "server thread start : " << port << endl;
    string start_server = "sh startServer.sh " + to_string(port) + " " + to_string(board_size) + " " + to_string(max_clients);
    for(int i=0;i<nfeatures;i++){
        cout << "in serverThread: " << wts_vec[i] << endl;
        start_server += " " + to_string(wts_vec[i]);
    }
    server_started=true;
    system(start_server.c_str());
}

void *clientThread(void* arg){
    sleep(1);
    while(!server_started);
    cout << "client thread start : " << port << endl;
    string start_client = "cd client && python client.py " + to_string(port);
    system(start_client.c_str());
}

void learning(){
    for(int i=1;i<=10;i++){
        for(int j=1;j<=10;j++){
            wts_vec[0] = i;
            wts_vec[1] = j;
            double cur_score=0;
            for(int k=0;k<5;k++){
                pthread_t server;
                pthread_t client;
                pthread_create(&server,NULL,serverThread,NULL);
                usleep(100);
                pthread_create(&client,NULL,clientThread,NULL);

                pthread_join(server,NULL);
                pthread_join(client,NULL);
                double game_score = 0;
                ifstream latest_scores ("latest_scores.txt");
                latest_scores >> game_score;
                cur_score += game_score;
            }
            vals[hashf(wts_vec)] = (cur_score/5.0);
        }
    }
    double max=0;
    for(auto entry: vals){
        if(entry.second >= max){
            max = entry.second;
            vector<double> wts1 = hash2state(entry.first);
            for(int i=0;i<nfeatures;i++) wts_vec[i] = wts1[i];
        }
    }
    cout << "max: " << max << endl;
    cout << "wts_vec: " << wts_vec[0] << " , " << wts_vec[1] << " , " << wts_vec[2] << " , " << wts_vec[3] << endl;
}

void appQLearn(){
    double max_score=(-1000);
    string best_state = "";
    while(1){
    // for(int i=0;i<10;i++){
        double wts_sum=0;
        for(int j=0;j<nfeatures;j++){
            if(wts_vec[j] > 0) wts_vec[j] = (1000000*wts_vec[j] + (rand()%(int(1000000*wts_vec[j])) - 500000*wts_vec[j]))/1000000;
            wts_sum += wts_vec[j];
        }
        wts_sum /= 100.0;
        if(wts_sum > 0) for(int j=0;j<nfeatures;j++) wts_vec[j] /= wts_sum;
        double cur_score=0;
        for(int j=0;j<5;j++){
            pthread_t server;
            pthread_t client;
            pthread_create(&server,NULL,serverThread,NULL);
            usleep(100);
            pthread_create(&client,NULL,clientThread,NULL);

            pthread_join(server,NULL);
            pthread_join(client,NULL);
            double game_score = 0;
            ifstream latest_scores ("latest_scores.txt");
            latest_scores >> game_score;
            cur_score += game_score;
        }
        cur_score /= 5.0;
        ofstream exp_scores ("exp_scores.txt", ios::app);
        exp_scores << cur_score << " " <<  hashf(wts_vec) << "\n";
        if(cur_score >= max_score){
            max_score = cur_score;
            best_state = hashf(wts_vec);
            ofstream out ("best_score.txt");
            out << max_score << " " << best_state << "\n";
            out.close();
        }
    }
}

int main(int argc, char** argv){
    board_size = atoi(argv[1]);
    // wts_vec[0] = stof(argv[1]);
    // wts_vec[1] = stof(argv[2]);
    srand(time(NULL));
    double wt1 = 100 / float(board_size*2);             // n-1 features for exact pal. , feature color, feature Distr. , n-1 features for expected pal.
    for(int i=0;i<board_size*2;i++){
        wts_vec.push_back(wt1);
    }
    nfeatures = wts_vec.size();
    appQLearn();
    return 0;
}