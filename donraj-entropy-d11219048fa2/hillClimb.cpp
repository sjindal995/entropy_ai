#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <cstring>
#include <sstream>
#include <fstream>
#include <map>
#include <vector>
#include <stack>
#include <tuple>
#include <algorithm>

using namespace std;

typedef tuple<int,double> topStates;
 
bool mycompare (const topStates &lhs, const topStates &rhs){
  return get<1>(lhs) > get<1>(rhs);
}

int port = 10000;
bool server_started = false;
int board_size = 5;
int max_clients = 2;
int nfeatures = 4;
double wts[4] = {1,1,1,1};
map<int, double> vals;
map<int, vector<int> > neighbours;
int best_score = 0;
int best_state = 1010101;


vector<int> loadNeighbours(int hash){
	std::vector<int> neighbours;
	int buff = hash;
	int pvm = 1; //place_value_multiplier

	for(int i=0; i<nfeatures; i++){
		int wt = buff%100;
		buff /= 100;
		if(wt<10)
			neighbours.push_back(hash+pvm);
		if(wt>1)
			neighbours.push_back(hash-pvm);
		pvm *= 100;
	}

	return neighbours;
}

int hashf(double i_wts[]){
	int hashv = 0;
	for(int i=0; i<nfeatures; i++){
		hashv *= 100;
		hashv += i_wts[i];
	}
	return hashv;
}

double* hash2state(int hashv){
	double* c_wts = new double[nfeatures];
	for(int pos = nfeatures-1; pos>=0; pos--){
		c_wts[pos] = hashv%100;
		hashv /= 100;
	}
	return c_wts;
}

void * serverThread(void* arg){
	port = (rand()%10000)+10000;
	cout << "server thread start : " << port <<endl;
	string start_server = "sh startServer.sh " + to_string(port) + " " + to_string(board_size) + " " + to_string(max_clients);
	for(int i=0; i<nfeatures; i++)
		start_server += " " + to_string(wts[i]);
	server_started = true;
	system(start_server.c_str());
}

void *clientThread(void* arg){
    sleep(1);
    while(!server_started);
    cout << "client thread start : " << port << endl;
    string start_client = "cd client && python client.py " + to_string(port);
    system(start_client.c_str());
}

int random_state_init(){
	int random_state = 0;

	for(int i=0; i<nfeatures; i++){
		int x = rand()%10 + 1;
		random_state += x; 
		if(i != nfeatures-1)
			random_state *= 100;
	}

	return random_state;
}

void hillClimb(){
	//hillClimb starting with a random state

	//init
	vector<int> explored;
	vector<topStates> states;
	int initial_state = random_state_init();
	cout << "initial_state: " << initial_state << endl;
	vector<int> neighbours = loadNeighbours(initial_state);
	explored.push_back(initial_state);
	//playing games for these neighbours
	for(int i=0; i<neighbours.size(); i++){
		int curr_state = neighbours[i];
		cout << "curr_state: " << curr_state << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" <<   endl;
		double* wts1 = hash2state(curr_state);
		for(int i=0;i<nfeatures;i++){
			wts[i] = wts1[i];
		}
		cout << endl;
		double cur_score = 0;
		/////use threads for each neighbor here
		//playing the game at the current state
		for(int k=0;k<5;k++){
                pthread_t server;
                pthread_t client;
                pthread_create(&server,NULL,serverThread,NULL);
                usleep(100);
                pthread_create(&client,NULL,clientThread,NULL);

                pthread_join(server,NULL);
                pthread_join(client,NULL);
                double game_score = 0;
                ifstream latest_scores ("latest_scores.txt");
                latest_scores >> game_score;
                cur_score += game_score;
            }
        int final_score = (cur_score/5.0);
        vals[curr_state] = final_score;
        states.push_back(make_tuple(curr_state,final_score));
	}
	exit(0);
	sort(states.begin(),states.end(),mycompare);
	//keeping top 10
	states.resize(10);

	bool stable = false;
	//hillclimbing starts
	for(int i=0; i<10; i++){
		//choosing the best state
		int curr_state = get<0>(states[0]);
		for(int top_counter=0; top_counter<10; top_counter++){
			std::vector<int>::iterator it;
			it = find (explored.begin(), explored.end(), curr_state);

			if(it == explored.end() ) {
				break;
			}
			else{
				if(top_counter == 9)
					stable = true;	
				curr_state = get<0>(states[i+1]);
			}
		}
		if(stable)
			break;

		vector<int> curr_neighbours = loadNeighbours(curr_state);
		//playing games for these neighbours
		for(int i=0; i<neighbours.size(); i++){
			int curr_state = neighbours[i];
			cout << "curr_state:   " << curr_state << "--------------------------------------------" <<  endl;
			//if curr state already played, then skip it
			if ( vals.find(curr_state) == vals.end() ) {
			  	// not found
			  	double* wts1 = hash2state(curr_state);
				for(int i=0;i<nfeatures;i++)
					wts[i] = wts1[i];
				double cur_score = 0;
				/////use threads for each neighbor here
				//playing the game at the current state
				for(int k=0;k<5;k++){
		                pthread_t server;
		                pthread_t client;
		                pthread_create(&server,NULL,serverThread,NULL);
		                usleep(100);
		                pthread_create(&client,NULL,clientThread,NULL);

		                pthread_join(server,NULL);
		                pthread_join(client,NULL);
		                double game_score = 0;
		                ifstream latest_scores ("latest_scores.txt");
		                latest_scores >> game_score;
		                cur_score += game_score;
		            }
		        int final_score = (cur_score/5.0);
		        vals[curr_state] = final_score;
		        states.push_back(make_tuple(curr_state,final_score));
			} 
			else {
			  	// found
				continue;				
			}
		}
		sort(states.begin(),states.end(),mycompare);
		//keeping top 10
		states.resize(10);
	}
	// if(stable)
	// 	restart;

	best_state = get<0>(states[0]);
	best_score = get<1>(states[0]);	

}

int main(){
	srand(time(NULL));
	
	ifstream best_log_file;
	best_log_file.open("best_till_now.txt");
	best_log_file >> best_state;
	best_log_file >> best_score;
	best_log_file.close();

	hillClimb();

	ofstream best_logfile;
	best_logfile.open("best_till_now.txt");
	best_logfile << best_state;
	best_logfile << " ";
	best_logfile << best_score;
	best_logfile.close();

}